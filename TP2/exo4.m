% Suite Exercice 4 TD2
% QUESTION a)

% N=500;
% F1=100;
% F2=105;
% Fe=3000;
% Te=1/Fe;
% t=(0:N-1)*Te;
% f=(0:N-1)*Fe/N;
% x=10*cos(2*pi*F1*t)+5*cos(2*pi*F2*t);
% X=fft(x)/N;
% figure;
% plot(f-Fe/2, fftshift(abs(X)), 'r');
% hold on;
% stem(f-Fe/2, fftshift(abs(X)));
% hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUESTION b)

% N2=600;
% f2=(0:N2-1)*Fe/N2;
% F1=100;
% F2=105;
% Fe=3000;
% Te=1/Fe;
% t=(0:N-1)*Te;
% f=(0:N-1)*Fe/N;
% x=10*cos(2*pi*F1*t)+5*cos(2*pi*F2*t);
% X2=fft(x,N2)/N;
% figure;
% plot(f2-Fe/2, fftshift(abs(X2)), 'r');
% hold on;
% stem(f2-Fe/2, fftshift(abs(X2)));
% hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUESTION c)
% Avec un nombre de points de 3000 on a un pas de 1Hz

% N2=3000;
% f2=(0:N2-1)*Fe/N2;
% F1=100;
% F2=105;
% Fe=3000;
% Te=1/Fe;
% t=(0:N-1)*Te;
% f=(0:N-1)*Fe/N;
% x=10*cos(2*pi*F1*t)+5*cos(2*pi*F2*t);
% X2=fft(x,N2)/N;
% figure;
% plot(f2-Fe/2, fftshift(abs(X2)), 'r');
% hold on;
% stem(f2-Fe/2, fftshift(abs(X2)));
% hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUESTION d)

N=600;
F1=100;
F2=105;
Fe=3000;
Te=1/Fe;
t=(0:N-1)*Te;
f=(0:N-1)*Fe/N;
x=10*cos(2*pi*F1*t)+5*cos(2*pi*F2*t);
X=fft(x)/N;
figure;
plot(f-Fe/2, fftshift(abs(X)), 'r');
hold on;
stem(f-Fe/2, fftshift(abs(X)));
hold off;
