% Exercice 7 Premier effet sonore (Reverse)

Fe=  44100 ; %choix fréquence échantillonnage
Nbits=8 ; % nombre de bits pour la numérisation
[x,Fe,Nbits]=wavread('son_TP1.wav') ; %acquisition du son  ? x
N=length(x) ;

% définition du vecteur temporel
t= (0 :N-1)/Fe;
figure(1)
plot(t,x)
xlabel('temps (s)') ; ylabel('Amplitude');

%Calcul de la fft
X=fft(x) ;
f=(0 :N-1)*Fe/N ;

%Visualisation fréquentielle
figure(2)
plot(f-Fe/2,fftshift(abs(X)))

%Ecoute du fichier .wav
%wavplay(x,Fe) ;
y = flipud(x);
wavplay(y,Fe)