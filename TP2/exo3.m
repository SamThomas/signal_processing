%Exercice 3
clear all;
close all;

N=500;
Fe=1000;
F1=10;
Te=1/Fe;

t=(0:N-1)*Te;
%s=5*cos(2*pi*F1*t);
s=5*square(2*pi*F1*t,25);
figure; 
plot(t,s);

f=(0:N-1)*Fe/N;
Sx=fft(s)/N;
figure;
plot(f, abs(Sx));
figure; plot(f-Fe/2,fftshift(abs(Sx)),'r');
hold on;
stem(f-Fe/2,fftshift(abs(Sx)));
hold off; 