
%Exercice 5: Ecouter une sinuso�de

Fe=  44100 ; %choix fr�quence �chantillonnage
%Fe=  34000;
%Fe=  96000;


L=1 ; % choix dur�e en seconde 
f0=450 ; %fr�quence en Hz

% d�finition du vecteur temporel
t= 0 :1/Fe :L ;

%cr�ation de la sinuso�de � �couter
x=2*sin(2*pi*f0*t) ;
%Calcul de la fft
N=length(x) ;
X=fft(x) ;
%y = 5*x;
f=(0 :N-1)*Fe/N ;

%Visualisation fr�quentielle
figure(1)
plot(f-Fe/2,fftshift(abs(X)))
%Ecoute de la sinusoide
wavplay(x,Fe) ;