% TP n�4
% Exercice 1: Filtrage de signaux

N=200;
f1=200;
f2=1000;
fe=8000; 

t=[0:N]/fe; 
x1=sin(2*pi*f2*t);
x2=sin(2*pi*f1*t);
figure(1)
plot(t,x1,t,x2)
[b,a]=butter(10,1/4);  % filtre de Butterworth d'ordre 1 avec 
                      %fn=Fc/(fe/2)=1/8
y1=filter(b,a,x1);
y2=filter(b,a,x2);
figure(2)
plot(t,y1,t,y2)
grid;


