*version 9.1 795828082
u 39
V? 2
R? 3
C? 2
L? 2
? 3
@libraries
@analysis
.TRAN 1 0 0 0
+0 0ns
+1 0.1ms
+3 0.1u
@targets
@attributes
@translators
a 0 u 13 0 0 0 hln 100 PCBOARDS=PCB
a 0 u 13 0 0 0 hln 100 PSPICE=PSPICE
a 0 u 13 0 0 0 hln 100 XILINX=XILINX
@setup
unconnectedPins 0
connectViaLabel 0
connectViaLocalLabels 0
NoStim4ExtIFPortsWarnings 1
AutoGenStim4ExtIFPorts 1
@index
pageloc 1 0 2714 
@status
n 0 113:00:16:11:49:24;1358333364 e 
s 2832 113:00:16:11:49:29;1358333369 e 
*page 1 0 970 720 iA
@ports
port 9 GND_EARTH 330 230 h
@parts
part 3 r 310 120 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R1
a 0 ap 9 0 15 0 hln 100 REFDES=R1
a 0 u 13 0 15 25 hln 100 VALUE=100k
part 4 r 540 160 d
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R2
a 0 ap 9 0 15 0 hln 100 REFDES=R2
a 0 u 13 0 15 25 hln 100 VALUE=10k
part 5 c 400 190 v
a 0 sp 0 0 0 10 hlb 100 PART=c
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=CK05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=C1
a 0 ap 9 0 15 0 hln 100 REFDES=C1
part 6 L 470 160 d
a 0 sp 0 0 0 10 hlb 100 PART=L
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=L2012C
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=L1
a 0 ap 9 0 15 0 hln 100 REFDES=L1
a 0 u 13 0 15 25 hln 100 VALUE=2.55m
part 2 VPULSE 230 170 h
a 0 a 0:13 0 0 0 hln 100 PKGREF=V1
a 1 ap 9 0 20 10 hcn 100 REFDES=V1
a 1 u 0 0 0 0 hcn 100 V1=0
a 1 u 0 0 0 0 hcn 100 V2=10
a 1 u 0 0 0 0 hcn 100 TD=0
a 1 u 0 0 0 0 hcn 100 TR=0
a 1 u 0 0 0 0 hcn 100 TF=0
a 1 u 0 0 0 0 hcn 100 PW=0.005m
a 1 u 0 0 0 0 hcn 100 PER=0.01m
part 1 titleblk 970 720 h
a 1 s 13 0 350 10 hcn 100 PAGESIZE=A
a 1 s 13 0 180 60 hcn 100 PAGETITLE=
a 1 s 13 0 300 95 hrn 100 PAGENO=1
a 1 s 13 0 340 95 hrn 100 PAGECOUNT=1
part 37 nodeMarker 540 120 d
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=1
part 38 nodeMarker 230 120 d
a 0 s 0 0 0 0 hln 100 PROBEVAR=
a 0 a 0 0 4 22 hlb 100 LABEL=2
@conn
w 19
a 0 up 0:33 0 0 0 hln 100 V=
s 540 200 540 230 18
s 540 230 470 230 20
s 330 230 230 230 22
a 0 up 33 0 280 229 hct 100 V=
s 230 230 230 210 23
s 400 230 330 230 30
s 400 190 400 230 28
s 470 230 400 230 33
s 470 220 470 230 31
w 15
a 0 up 0:33 0 0 0 hln 100 V=
s 350 120 400 120 14
s 540 120 540 160 16
s 400 120 470 120 27
s 400 120 400 160 25
s 470 120 540 120 36
a 0 up 33 0 505 119 hct 100 V=
s 470 160 470 120 34
w 11
a 0 up 0:33 0 0 0 hln 100 V=
s 230 170 230 120 10
s 230 120 310 120 12
a 0 up 33 0 270 119 hct 100 V=
@junction
j 310 120
+ p 3 1
+ w 11
j 350 120
+ p 3 2
+ w 15
j 540 160
+ p 4 1
+ w 15
j 540 200
+ p 4 2
+ w 19
j 330 230
+ s 9
+ w 19
j 400 160
+ p 5 2
+ w 15
j 400 120
+ w 15
+ w 15
j 400 190
+ p 5 1
+ w 19
j 400 230
+ w 19
+ w 19
j 470 220
+ p 6 2
+ w 19
j 470 230
+ w 19
+ w 19
j 470 160
+ p 6 1
+ w 15
j 470 120
+ w 15
+ w 15
j 540 120
+ p 37 pin1
+ w 15
j 230 120
+ p 38 pin1
+ w 11
j 230 170
+ p 2 +
+ w 11
j 230 210
+ p 2 -
+ w 19
@attributes
a 0 s 0:13 0 0 0 hln 100 PAGETITLE=
a 0 s 0:13 0 0 0 hln 100 PAGENO=1
a 0 s 0:13 0 0 0 hln 100 PAGESIZE=A
a 0 s 0:13 0 0 0 hln 100 PAGECOUNT=1
@graphics
